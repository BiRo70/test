#!/usr/bin/perl
#
# Perl version: v5.22.1
# Date: 2 October 2016
#        
# Test directive:       
# We have some customer records in a text file (customers.json) -- one customer per line, JSON-encoded. 
# We want to invite any customer within 100km of our Dublin office for some food and drinks on us.
# Write a program that will:
#  read the full list of customers and 
#  output the names and user ids of matching customers (
#  within 100km), 
#  sorted by User ID (ascending).
#     You can use the first formula from this Wikipedia article to calculate distance. Don't forget, you'll need to convert degrees to radians.
#     The GPS coordinates for our Dublin office are 53.3381985, -6.2592576.
#     You can find the Customer list here.
# 
#
# assumptions: the input file has valid fields and data
#              there is enough memory to hold the array containing at least as much records/lines as the input file size

use strict;
use warnings;
use JSON;
use constant PI => 4 * atan2(1, 1);


# parse arguments for file names
my ($filein, $fileout) = @ARGV;

if (not defined $filein) {
    die "Error: missing argument source file name\nExample: $0 filename.json results.txt\nA missing second argument implies stdout as output";
}

# declare file handlers
my $fhi;
my $fho;

# open input
open ($fhi, '<:encoding(UTF-8)', $filein)
    or die "Could not open file '$filein'\n$!\n";  # $! is the system error variable.

# open output
if (defined $fileout) {
    unless (open ($fho, '>:encoding(UTF-8)', $fileout)) {
        warn "Warning: Could not write to file. Read only? Redirecting to stdout\n";
        $fho = \*STDOUT;
    }
} else {
  	warn "Warning: no output file given! Redirecting to stdout\n";
  	$fho = \*STDOUT;
}

# viariables defining the coordinates of the office and the distance radius 
my $officelat = 53.3381985;
my $officelon = -6.2592576;
my $maxdist = 100;

# Array containing the sorted list of users data + distance from office
# Note: if there is not enough memory to hold the array of hashes consider
#       removing the part of the hashes not necessary for the output.
#       possibly save chunks of data big enough not to fill the memory on
#       temporary files and merge sort them at the end
my @sortedlist;

# begin main iteration parsing the input file to the end and insert sorting into the array.
while (my $row = <$fhi>) {
    # remove trailing newlines
    chomp $row;
    # use JSON module to convert row of values to perl hash
    # consider adding a control on the validity of the fields and if decode_json returns an exception.
    my $userhash = decode_json($row);
    # calculate the distance from the office
    # to be introduced a control for the latitude range to be between -90 and +90 degrees and longitude range between -180 and +180 degrees.
    my $dist = distance($userhash->{'latitude'}, $userhash->{'longitude'},$officelat,$officelon);
    # consider a valid entry if the distance is less or equal to 100Km 
    if ($dist <= $maxdist) {
    	# append the distance to the user hash
        $userhash->{'distance'} = sprintf("%.2f",$dist);
        # insert the hash in the array in ordered position by user id.
        # add hash at array beginning
        unshift @sortedlist, $userhash;
        # bubble sort down the array assuming user_id is unique.
        for my $x (0..@sortedlist-2) {
            if (ref $sortedlist[$x+1] eq 'HASH'){
                last if $sortedlist[$x]->{'user_id'} <= $sortedlist[$x+1]->{'user_id'};
                ($sortedlist[$x],$sortedlist[$x+1]) = ($sortedlist[$x+1],$sortedlist[$x])        	
	    }        
        }

    }
    # here code what is necessary if array is going to be bigger than the memory
}

# quickly print the result data (user_id, name and distance) in json format.
for my $x (@sortedlist){
	print $fho "\{\"user_id\": $x->{'user_id'}, ";
	print $fho "\"name\": \"$x->{'name'}\", ";
	print $fho "\"distance\": $x->{'distance'}";
	print $fho "\}\n"
}

#        	print "$sortedlist[$x]->{'user_id'}\n";

#====================#
# functions sections #
#====================#

#===
# Function to calculate the distance between two coordinates as described
# here https://en.wikipedia.org/wiki/Great-circle_distance
# values passed to the function are in degrees and need to be converted to radians for the formula.
# it considers the earth mean radius of 6371 kilometers
#===
sub distance {
    my ($degphi1, $deglambda1, $degphi2, $deglambda2) = @_;
    my $dist = acos(sin(degtorad($degphi1)) * sin(degtorad($degphi2)) + cos(degtorad($degphi1)) * cos(degtorad($degphi2)) * cos(degtorad($deglambda1 - $deglambda2))) * 6371;
    return ($dist);
}

#===
# Function to convert decimal degrees to radians
#===
sub degtorad {
    my ($deg) = @_;
    return ($deg * PI / 180);
}

#===
# Function to calculate the arch cosinus of a rad
#===
sub acos {
    my ($rad) = @_;
    my $ret = atan2(sqrt(1 - $rad**2), $rad);
    return $ret;
}
