#!/usr/bin/perl
#
# Perl version: v5.22.1
# Date: 2 October 2016
# Desc.: Function that will flatten an array of arbitrarily nested arrays of
#        integers into a flat array of integers.
#        Two versions of the same function are coded below. The first one is
#        coded by me, while the other, which is a more concise version of the
#        same recursion, is taken from here:
#             https://rosettacode.org/wiki/Flatten_a_list
#



use strict;
use warnings;

# assuming a well formatted nested array of values, considering empty arrays too.
my @array = ([], [[1], 2, [[3, 4], 5], [[[],[]],[]], [[[6]]], 7, 8, []]);

# let's print it formatted for the human eye
print "\n";
print "flatten1\n";
print "(".join(", ", flatten(\@array)).")\n"; # \@ passes the reference to the array
print "\n";

print "\n";
print "flatten2\n";
print "(".join(", ", flatten2(\@array)).")\n";
print "\n";

# recursive function flatten
sub flatten {
    # assign the list of values (@_) to list nestedarray, which is a reference to an array
    my ($nestedarray) = @_;
    # declare local array that will contain the resulting flatened array to returm at the end of the function
    my @flattened;
    # for each element of the nested array referred by $a do: (if empty it won't go into the cycle and result won't append anything)
    for my $a (@$nestedarray) {
        # if $a is an array
        if ( ref $a eq 'ARRAY') {
            # then recursively flatten this array too and append to the result
            push(@flattened, flatten($a));
        } else {
            # otherwise it is a value so append it to the result
            push(@flattened, $a);
        }
    }
    return @flattened;
}


# shorter recursive function using map.
sub flatten2 {
    map { ref eq 'ARRAY' ? flatten2 (@$_) : $_ } @_
}

# use Data::Dump qw(dump);
# dump(@FlatArray);

# echo "[[1], 2, [[3, 4], 5], [[[],[]],[]], [[[6]]], 7, 8, []]" | sed -r 's/(\[|\])//g;s/ ,{1,}//g'