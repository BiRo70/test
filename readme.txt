The Test

  1  What's your proudest achievement? It can be a personal project or something you've worked on professionally. Just a short paragraph is fine, but I'd love to know why you're proud of it, what impact it had (If any) and any insights you took from it.
  2  Write a function that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] → [1,2,3,4]. If the language you're using has a function to flatten arrays, you should pretend it doesn't exist.
  3  We have some customer records in a text file (customers.json) -- one customer per line, JSON-encoded. We want to invite any customer within 100km of our Dublin office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).

        You can use the first formula from this Wikipedia article to calculate distance. Don't forget, you'll need to convert degrees to radians.
        The GPS coordinates for our Dublin office are 53.3381985, -6.2592576.
        You can find the Customer list here.

⭑ Please don’t forget, your code should be production ready, clean and tested!

Directory Part1 contains the text file describing the proudes achievement.
Directory Part2 contains the excutable perl script with the function to flatten an array. Change the variable @array to test the function
Directory Part3 contains the excutable perl script to find the customer in a 100Km radius from the HQ. A test.sh file can be run which will call the perl script with the proper parameters. Otherwise the perl script distcalc.pl can be run with passing the source json file contaning the customer DB and the destination file to write the results into.


some reference

http://www.movable-type.co.uk/scripts/latlong.html
"calculate distance between two latitude longitude points perl"
http://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
http://www.geodatasource.com/developers/perl
http://perldoc.perl.org/Math/Trig.html


http://www.perlmonks.org/?node_id=308274
https://rosettacode.org/wiki/Flatten_a_list

